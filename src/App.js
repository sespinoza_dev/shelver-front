import React from 'react';
import Header from './Header';
import Footer from './Footer';
import Content from './AppStyled';

const App = () => (
  <Content>
    <Header />
    <Footer />
  </Content>
);

export default App;
