import styled from 'styled-components';

export default styled.li`
  margin: 0;
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  background-color: #f4f1ea;
`;
