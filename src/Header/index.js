import React from 'react';
import {
  HeaderWrapper,
  HeaderContainer,
  Title,
  Icon,
  IconContainer,
} from './styles';

import shelf from './img/icons8-book-shelf-30.png';

const Header = () => (
  <HeaderWrapper>
    <HeaderContainer>
      <IconContainer>
        <Icon
          alt="shelf logo"
          src={shelf}
        />
        <Title>SHELVER</Title>
      </IconContainer>
    </HeaderContainer>
  </HeaderWrapper>
);

export default Header;
