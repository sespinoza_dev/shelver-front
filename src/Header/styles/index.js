import styled from 'styled-components';

export const HeaderWrapper = styled.nav`
  display: flex;
  justify-content: space-around;
  background-color: #1e222d;
  opacity: 0.8;
  color: #a5a3a9;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.25);
`;

export const HeaderContainer = styled.div``;

export const IconContainer = styled.div`
  display: flex;
  flex-wrap: no-wrap;
  margin: 5px;
`;

export const Title = styled.h1`
  margin: 0px 0px 0px 3px;
  padding: 0px;
`;

export const Icon = styled.img`
  height: 30px;
  width: 30px;
`;
