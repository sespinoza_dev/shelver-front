import styled from 'styled-components';

export default styled.footer`
  display: flex;
  margin-top: auto;
  justify-content: space-around;
  color: #ffffff;
  padding: 20px;
  background-color: #1e222d;
  opacity: 0.9;
`;
