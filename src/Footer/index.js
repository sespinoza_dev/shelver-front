import React from 'react';
import FooterWrapper from './styles';

const Footer = () => (
  <FooterWrapper>
    Copyright &copy; 2020 S.Espinoza & D.Salazar.
  </FooterWrapper>
);

export default Footer;
